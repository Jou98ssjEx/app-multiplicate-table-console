const argv = require('yargs')
    .option('b', {
        alias: 'base',
        type: 'number',
        demandOption: true,
        describe: 'Multiplication number'
    })
    .option('i', {
        alias: 'item',
        type: 'number',
        demandOption: true,
        describe: 'Times to multiply'
    })
    .option('l', {
        alias: 'listar',
        type: 'boolean',
        default: false,
        describe: 'Multiplication table list'
            // demandOption: true
    })
    .check((argv, options) => {
        if (isNaN(argv.b)) {
            throw 'The base must be a number'
        }
        return true
    })
    .argv;

module.exports = argv;