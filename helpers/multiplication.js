const fs = require('fs');
const colors = require('colors');

const createFile = async(base, item, list) => {

    try {

        let out = '';
        for (let i = 0; i <= item; i++) {

            out += ` ${base} X ${i} = ${base*i} \n `;
        }

        fs.writeFileSync(`./out/Table of ${base}.txt`, out);

        if (list) {

            console.log(colors.red("============================="));
            console.log(colors.bold(`========Table of ${colors.magenta(base)}=========`));
            console.log(colors.red("============================="));

            console.log(out);
        }

        return `Table of ${base}.txt create`.green;

    } catch (error) {
        throw error
    }
}


module.exports = {
    createFile
}